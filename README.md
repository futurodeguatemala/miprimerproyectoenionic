Sitios :
    Node: https://nodejs.org
    Ionic: https://ionicframework.com
    Android Studio: https://developer.android.com/studio/index.html?hl=es-419
    JDK: http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html

Comandos:

    Instalar ionic y cordova: 
        npm install -g cordova ionic
    Crear aplicación:
        ionic start myApp sidemenu
    Iniciar servidor:
        ionic serve
    Ver listado de plataformas:
        ionic cordova platform list
    Instalar plataforma android:
        ionic cordova platform add android
    Ver dispositivos conectados con DEPURACION HABILITADA:
        adb devices
    Crear archivo .apk de android:
        ionic cordova build android
    Correr el proyecto en un dispositivo:
        ionic cordova run android



